/* eslint-disable react/prop-types */
import React from "react";
import {Row, Container, Col} from "react-bootstrap";
import { formatNumber } from "../helpers";
import "./styles.scss";
const Data = ({data,vaccineData}) => {

    return (
        <Container className="data">

            <Row className=" data-row-1">
                <h1>{formatNumber(data.cases)}</h1>
                <span className="h5">Cummulatevely</span>
            </Row>
    
            <Row className="data-row-2 justify-content-center">
                <Col className="data-col m-auto">
                    <h1>-</h1>
                    <span>Tests</span>
                    <h3>{formatNumber(data.tests)}</h3>
                </Col>
                <Col className="data-col m-auto">
                    <h1>{formatNumber(data.todayCases)}</h1>
                    <span>Positive Cases</span>
                    <h3>{formatNumber(data.cases)}</h3>
                </Col>
                <Col className="data-col m-auto">
                    <h1>{formatNumber(data.todayCases)}</h1>
                    <span>Hospitalized</span>
                    <h3>{formatNumber(data.active)}</h3>
                </Col>
                <Col className="data-col m-auto">
                    <h1>{formatNumber(data.todayRecovered)}</h1>
                    <span>Recovered</span>
                    <h3>{formatNumber(data.recovered)}</h3>
                </Col>
             
                <Col className="data-col m-auto">
                    <h1>{formatNumber(data.todayDeaths)}</h1>
                    <span>Deaths</span>
                    <h3>{formatNumber(data.deaths)}</h3>
                </Col>
                {vaccineData?.timeline ? 
                    <Col className="data-col m-auto">
                        <h1>{vaccineData?.timeline[0].daily || "-"}</h1>
                        <span>Vaccinated</span>
                        <h3>{vaccineData?.timeline[0].total || "-"}</h3>
                    </Col> : 
                    <Col className="data-col m-auto">  <h1>-</h1>
                        <span>Vaccinated</span>
                        <h3>-</h3></Col>
                  
                }
               
            </Row>

        </Container>
    );
};
export default Data;