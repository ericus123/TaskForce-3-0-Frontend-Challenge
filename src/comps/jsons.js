import ReactCountryFlag from "react-country-flag";
import { getData } from "country-list";
import states from "./states.json";

export const Countries = () => {
    let options = getData();
    options.forEach((country) =>{ country.label =  <> <ReactCountryFlag
        countryCode={country.code}
        svg
        style={{
            width: "2em",
            height: "2em",
        }}
        title={country.name}
    /> {country.name}</>, country.type = "country";});
    return {data:options,default:{ label:  <> <ReactCountryFlag
        countryCode="RW"
        svg
        style={{
            width: "2em",
            height: "2em",
        }}
    /> Rwanda</>, code: "RW",value:"Rwanda" }};

};
export const States = () => {
    let statesData = states;
    states.forEach((country) => { country.label = country.name, country.type="state"; });
    return {
        data:statesData,default: statesData[0]
    };
};