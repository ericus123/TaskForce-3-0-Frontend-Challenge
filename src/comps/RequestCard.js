/* eslint-disable react/prop-types */
import React from "react";
import {useState, useEffect} from "react";
import { useDispatch} from "react-redux";
import { GetCountryData, GetStateData, GetVaccineCountryData } from "../redux/actions";
import {Row, Col, Form, Button, Spinner} from "react-bootstrap";
import CountrySelector from "./CountrySelector";
import "./styles.scss";


const RequestCard =   ({title, select_data,defaultData, loading,changeCountry,changeState,handleSubmit}) => {

    const dispatch = useDispatch();
    const [country,setCountry] = useState("Rwanda");
    const [state,setState] = useState("Texas");

    useEffect(() => {
        dispatch(GetCountryData(country));
        dispatch(GetStateData(state));
        dispatch(GetVaccineCountryData(country,1));
    },[]);
   
    const handleChange = (value) => {

        if(value.type === "country"){
            setCountry(value);
            changeCountry(value); 
        }else{
            setState(value);
            changeState(value);
        }
       
    };

   
    return (
        <div className="mb-5" style={{marginBottom: "50%"}}>
            <div className="justify-content-md-center req-card-header mt-4">
                <h1 className="updates-title" style={{color:"white"}}>UPDATES</h1>
                <p>{title}</p>
            </div>
            
            <Form onSubmit={handleSubmit}>
                <Row  className="justify-content-center no-gutters m-2">
                    <Col  sm="3" className="mr-0 px-1">
                       
                        <CountrySelector defaultData={defaultData} options={select_data} disabled={loading} handleChange={handleChange}/>
                    </Col>
                    <Col   sm="3" className="mr-0 px-1" style={{borderRadius:"0px", background:"white"}} >
                        <input className="text-center form-control"  disabled={loading} required style={{border:"none"}} type="date" name="date"/>
                   
                    </Col> 
                    <Col sm="1" className="mr-0 px-1" style={{borderRadius:"0px"}}>
                        <Button disabled={loading} style={{background:"#1E776E"}} type="submit">
                            {loading ? (
                                <Spinner size="sm" animation="border"></Spinner>
                            ) : (
                                "Submit"
                            )}
                        </Button>
                    </Col>
                    
                </Row>
            </Form>
            <br/>
            <br/>
        </div>
    );
};
export default RequestCard;