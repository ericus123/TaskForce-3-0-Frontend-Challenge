/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */


import Select from "react-select";

import React from "react";

const CountrySelector = ({handleChange,options,defaultData}) => {
    return (
        <Select defaultValue={defaultData} options={options} onChange = {(value) => handleChange(value)} />
    );

};
export default CountrySelector;