import React from "react";
import Contact from "./comps/Contact";
import Data from "./comps/Data";
import DataContinent from "./comps/DataContinents";
import Footer from "./comps/Footer";
import Navigation from "./comps/Navbar";
import ProfileCard from "./comps/ProfileCard";
import RequestCard from "./comps/RequestCard";
import { useDispatch,useSelector } from "react-redux";

import "./styles/index.scss";
import { Countries, States } from "./comps/jsons";
import { GetCountryData, GetStateData, GetVaccineCountryData, GetVaccineStateData } from "./redux/actions";
import { useState } from "react";


const Container = () => {
    const  countryData = useSelector((state) => state.CovidDataReducer.countryData);
    const  vaccineCountryData = useSelector((state) => state.CovidDataReducer.vaccineCountryData);
    const  vaccineStateData = useSelector((state) => state.CovidDataReducer.vaccineStateData);
    const  stateData = useSelector((state) => state.CovidDataReducer.stateData);
    // const  vaccineStateData = useSelector((state) => state.CovidDataReducer.vaccineStateData);
    const  countryDataIsLoading = useSelector((state) => state.CovidDataReducer.countryDataIsLoading);
    const  stateDataIsLoading = useSelector((state) => state.CovidDataReducer.stateDataIsLoading);
    const [country,setCountry] = useState("Rwanda");
    const [state,setState] = useState("Texas");

    const dispatch = useDispatch();
    const changeCountry = (value) => {
        setCountry(value); 
        console.log("value is" + value.name);
    };
    
    const changeState = (value) => {
        setState(value.name);
    };
   
    
    const getDays = (date) => {
        const date1 = new Date(date);
        const date2 = new Date();
        const days = parseInt((date2.getTime() - date1.getTime())/(1000 * 3600 * 24));
        return days;
    };
    const handleSubmitCountry = (e) => {
        e.preventDefault();
        dispatch(GetVaccineCountryData(country.name,getDays(e.target.date.value)));
        dispatch(GetCountryData(country.name));
        
    };
    const handleSubmitState = (e) => {
        e.preventDefault();
        dispatch(GetVaccineStateData(state,getDays(e.target.date.value)));
        dispatch(GetStateData(state));
    };



    return (
        <div className="">
            <div className="upper-page">
                <Navigation/>
                <RequestCard handleSubmit={handleSubmitCountry}  changeCountry={changeCountry} changeState={changeState}   change data={countryData} loading={countryDataIsLoading} select_data={Countries().data} defaultData={Countries().default}  title="Search a country"/>
            </div>
            <Data data={countryData} vaccineData={vaccineCountryData}/>

          
      
            <div className="upper-page">
                <br/>
                <RequestCard vaccineData={vaccineStateData} handleSubmit={handleSubmitState}  changeCountry={changeCountry}  changeState={changeState} dataAction={GetCountryData} vaccineAction={GetVaccineCountryData}  data={stateData} loading={stateDataIsLoading} select_data={States().data}   defaultData={States().default} title="Search a state" />
              
            </div>
            <Data data={stateData} vaccineData={vaccineStateData}/>
            <DataContinent/>
            <ProfileCard/>
            <Contact/>
            <Footer/>
        </div>
    );
};
export default Container;